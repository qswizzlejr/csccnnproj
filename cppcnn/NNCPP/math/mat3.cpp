#include "mat3.h"

namespace _math
{
	mat3::mat3(int x, int y, int z, float f)
	{
		elem = std::vector< mat2 >(z, mat2(x, y, f));
		_size = { x, y, z };
	}

	mat3::mat3(index3 x, float f)
	{
		elem = std::vector< mat2 >(x.z, mat2(x.x, x.y, f));
		_size = x;
	}

	mat3::~mat3()
	{
	}

	float mat3::sum(mat3 _mat)
	{
		float sum = 0;
		for (mat2 i : _mat.elem)
			for (int j = 0; j < i.size().y; j++)
				for (int k = 0; k < i.size().x; k++)
					sum += i[j][k];
		return sum;
	}

	float mat3::find_max_value(mat3 _mat)
	{
		float max = 0;
		for (mat2 i : _mat.elem)
			for (int j = 0; j < i.size().y; j++)
				for (int k = 0; k < i.size().x; k++)
					if (i[j][k] > max)
						max = i[j][k];
		return max;
	}

	std::vector<float> mat3::_3d_1d(mat3 _mat)
	{
		std::vector<float> res(_mat.size().x * _mat.size().y * _mat.size().z);
		for (int i = 0; i < _mat.size().z; i++)
			for (int j = 0; j < _mat.size().y; j++)
				for (int k = 0; k < _mat.size().x; k++)
					res[k + _mat.size().y * (i + _mat.size().z * i)] = _mat[i][j][k];
		return res;
	}

	mat3 mat3::_1d_3d(std::vector<float> res, index3 _size)
	{
		mat3 _mat = mat3(_size);
		for (int z = 0; z < _mat.size().z; z++)
			for (int y = 0; y < _mat.size().y; y++)
				for (int x = 0; x < _mat.size().x; x++)
					_mat[z][y][x] = res[z + _mat.size().y*y + _mat.size().y*_mat.size().z*x];
		return _mat;
	}

	index3 mat3::find_max_loc(mat3 _mat)
	{
		float max = 0;
		index3 loc = { 0,0,0 };
		for (int i = 0; i < _mat.size().z; i++)
			for (int j = 0; j < _mat.size().y; j++)
				for (int k = 0; k < _mat.size().x; k++)
					if (_mat[i][j][k] > max)
						loc = { i, j, k };
		return loc;
	}

	index3 mat3::size()
	{
		return this->_size;
	}

	mat3 mat3::z_fill(mat3 _mat, int x)
	{
		mat3 res = mat3({ _mat.size().x + 2 * x });
		for (int i = 0; i < _mat.size().z; i++)
			for (int j = 0; j < _mat.size().y; j++)
				for (int k = 0; k < _mat.size().x; k++)
					res[i + x][j + x][k + x] = _mat[i][j][k];
		return res;
	}

	void mat3::z_fill(int x)
	{
		mat3 res = mat3({ size().x + 2 * x });
		for (int i = 0; i < size().z; i++)
			for (int j = 0; j < size().y; j++)
				for (int k = 0; k < size().x; k++)
					res[i + x][j + x][k + x] = (*this)[i][j][k];
		(*this) = res;
	}

	mat3 mat3::make_random_mat(int x, int y, int z)
	{
		//setup random generator
		std::default_random_engine rand;
		std::uniform_real_distribution<float> dist(-5, 5);

		//create new matrix with random values
		mat3 res = mat3(x, y, z);
		for (int i = 0; i < z; i++)
			for (int j = 0; j < y; j++)
				for (int k = 0; k < x; k++)
				res[{i, j, k}] = dist(rand);
		return res;
	}

	mat3 mat3::sub_mat(mat3 _mat, int x1, int y1, int z1, int x2, int y2, int z2)
	{
		mat3 res = mat3(x2 - x1 + 1, y2 - y1 + 1, z2 - z1 + 1);
		for (int i = 0; i < z2 - z1 + 1; i++)
			for (int j = 0; j < y2 - y1 + 1; j++)
				for (int k = 0; k < x2 - x1 + 1; k++)
					if (i + z1 < _mat.size().z && j + y1 < _mat.size().y && k + x1 < _mat.size().x)
						res[i][j][k] = _mat[i + z1][j + y1][k + x1];
					else
						res[i][j][k] = 0;
		return res;
	}

	mat3 mat3::sub_mat(mat3 _mat, index3 start, index3 end)
	{
		mat3 res = mat3(end.x - start.x + 1, end.y - start.y + 1, end.z - start.z + 1);
		for (int i = 0; i < end.z - start.z + 1; i++)
			for (int j = 0; j < end.y - start.y + 1; j++)
				for (int k = 0; k < end.x - start.x + 1; k++)
					if (i + start.z < _mat.size().z && j + start.y < _mat.size().y && k + start.z < _mat.size().x)
						res[i][j][k] = _mat[i + start.z][j + start.y][k + start.x];
					else
						res[i][j][k] = 0;
		return res;
	}

	mat3 mat3::apply_func(mat3 _mat, float(*f)(float, bool), bool deriv)
	{
		mat3 res = mat3(_mat.size());
		for (int i = 0; i < _mat.size().z; i++)
			for (int j = 0; j < _mat.size().y; j++)
				for (int k = 0; k < _mat.size().x; k++)
					res[i][j][k] = f(_mat[i][j][k], deriv);
		return res;
	}

	void mat3::apply_func(float(*f)(float, bool), bool deriv)
	{
		mat3 res = mat3(size());
		for (int i = 0; i < size().z; i++)
			for (int j = 0; j < size().y; j++)
				for (int k = 0; k < size().x; k++)
					res[i][j][k] = f((*this)[i][j][k], deriv);
		(*this) = res;
	}

	void mat3::push_back(mat2 x)
	{
		this->_size.z += 1;
		this->elem.push_back(x);
	}

	void mat3::remove_first()
	{
		this->_size.z -= 1;
		this->elem.erase(elem.begin());
	}

	mat2 mat3::pop_back()
	{
		this->_size.z -= 1;
		mat2 res = this->elem.back();
		this->elem.pop_back();
		return res;
	}

	mat2& mat3::operator[](int x)
	{
		return this->elem[x];
	}

	float & mat3::operator[](index3 x)
	{
		return this->elem[x.z][x.y][x.x];
	}

	mat3 operator*(mat3 a, mat3 b)
	{
		mat3 res = mat3(a.size());
		for (int i = 0; i < a.size().z; i++)
			for (int j = 0; j < a.size().y; j++)
				for (int k = 0; k < a.size().x; k++)
					res[i][j][k] = a[i][j][k] * b[i][j][k];
		return res;
	}

	mat3 operator*(mat3 a, float b)
	{
		mat3 res = mat3(a.size());
		for (int i = 0; i < a.size().z; i++)
			for (int j = 0; j < a.size().y; j++)
				for (int k = 0; k < a.size().x; k++)
					res[i][j][k] = a[i][j][k] * b;
		return res;
	}

	std::ostream & operator<<(std::ostream & os, mat3 x)
	{
		for (int i = x.size().z - 1; i >= 0; i--)
			os << x[i] << std::endl;
		return os;
	}
}