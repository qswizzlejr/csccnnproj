#pragma once
#include <vector>

#include "../math/mat3.h"
#include "../math/_math.h"

namespace _nn
{
	class neural_net
	{
	public:
		neural_net(std::vector<int>, bool ReLU = false, std::string name = "default_name");
		~neural_net();

		//_math::mat3
		_math::mat3 dot_with_func(_math::mat3 x, _math::mat3 y, float(*f)(float, bool), bool deriv = false);



		friend std::ostream& operator<<(std::ostream&, neural_net);

	private:
		//bool
		bool relu;
		
		//std::string
		std::string name;
		
		//int
		int layer_count;

		//mat3
		_math::mat3 last_error;
		_math::mat3 weights;

	};
}