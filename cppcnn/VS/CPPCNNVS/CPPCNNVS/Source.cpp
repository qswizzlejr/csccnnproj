#include <vector>						//dynamic arrays
#include <iostream>						//IO
#include "neural_network/neural_net.h"	//neural network
#include <conio.h>

using namespace _math;
using namespace _nn;
//using namespace _cnn;

float woot(float, bool);

int main()
{	
	neural_net x({ 2, 3, 5, 3, 5, 1 });

	float s = 0;
	for (int i = 0; i < 80000 && !_kbhit(); i++)
	{
		s = x.train({ .2f, 1.f }, { 1 })[0];
		x.train({ 1.f, .2f }, { 0 });
		if (i % 200000)
			std::cout << s << "\t" << i << std::endl;
	}

	std::cout << "*********Results*********" << std::endl;
	for(int i = 0; i < x.size(); i++)
	std::cout << x.get_output({ .2f, 1.f })[i] << std::endl;
	for (int i = 0; i < x.size(); i++)
		std::cout << x.get_output({ 1.f, .2f })[i] << std::endl;


	std::cin.get();
	return 0;
}

float woot(float x, bool y)
{
	return x;
}