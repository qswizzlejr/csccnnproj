// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CSC546_CSC546WheelFront_generated_h
#error "CSC546WheelFront.generated.h already included, missing '#pragma once' in CSC546WheelFront.h"
#endif
#define CSC546_CSC546WheelFront_generated_h

#define CSC546_Source_CSC546_CSC546WheelFront_h_12_RPC_WRAPPERS
#define CSC546_Source_CSC546_CSC546WheelFront_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CSC546_Source_CSC546_CSC546WheelFront_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCSC546WheelFront(); \
	friend struct Z_Construct_UClass_UCSC546WheelFront_Statics; \
public: \
	DECLARE_CLASS(UCSC546WheelFront, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(UCSC546WheelFront)


#define CSC546_Source_CSC546_CSC546WheelFront_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUCSC546WheelFront(); \
	friend struct Z_Construct_UClass_UCSC546WheelFront_Statics; \
public: \
	DECLARE_CLASS(UCSC546WheelFront, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(UCSC546WheelFront)


#define CSC546_Source_CSC546_CSC546WheelFront_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSC546WheelFront(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSC546WheelFront) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSC546WheelFront); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSC546WheelFront); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSC546WheelFront(UCSC546WheelFront&&); \
	NO_API UCSC546WheelFront(const UCSC546WheelFront&); \
public:


#define CSC546_Source_CSC546_CSC546WheelFront_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSC546WheelFront(UCSC546WheelFront&&); \
	NO_API UCSC546WheelFront(const UCSC546WheelFront&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSC546WheelFront); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSC546WheelFront); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCSC546WheelFront)


#define CSC546_Source_CSC546_CSC546WheelFront_h_12_PRIVATE_PROPERTY_OFFSET
#define CSC546_Source_CSC546_CSC546WheelFront_h_9_PROLOG
#define CSC546_Source_CSC546_CSC546WheelFront_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_RPC_WRAPPERS \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_INCLASS \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CSC546_Source_CSC546_CSC546WheelFront_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_INCLASS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546WheelFront_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CSC546_Source_CSC546_CSC546WheelFront_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
