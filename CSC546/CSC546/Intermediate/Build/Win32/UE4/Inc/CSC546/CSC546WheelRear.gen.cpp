// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CSC546/CSC546WheelRear.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCSC546WheelRear() {}
// Cross Module References
	CSC546_API UClass* Z_Construct_UClass_UCSC546WheelRear_NoRegister();
	CSC546_API UClass* Z_Construct_UClass_UCSC546WheelRear();
	PHYSXVEHICLES_API UClass* Z_Construct_UClass_UVehicleWheel();
	UPackage* Z_Construct_UPackage__Script_CSC546();
// End Cross Module References
	void UCSC546WheelRear::StaticRegisterNativesUCSC546WheelRear()
	{
	}
	UClass* Z_Construct_UClass_UCSC546WheelRear_NoRegister()
	{
		return UCSC546WheelRear::StaticClass();
	}
	struct Z_Construct_UClass_UCSC546WheelRear_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCSC546WheelRear_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVehicleWheel,
		(UObject* (*)())Z_Construct_UPackage__Script_CSC546,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCSC546WheelRear_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CSC546WheelRear.h" },
		{ "ModuleRelativePath", "CSC546WheelRear.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCSC546WheelRear_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCSC546WheelRear>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCSC546WheelRear_Statics::ClassParams = {
		&UCSC546WheelRear::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UCSC546WheelRear_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCSC546WheelRear_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCSC546WheelRear()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCSC546WheelRear_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCSC546WheelRear, 4060558038);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCSC546WheelRear(Z_Construct_UClass_UCSC546WheelRear, &UCSC546WheelRear::StaticClass, TEXT("/Script/CSC546"), TEXT("UCSC546WheelRear"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCSC546WheelRear);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
