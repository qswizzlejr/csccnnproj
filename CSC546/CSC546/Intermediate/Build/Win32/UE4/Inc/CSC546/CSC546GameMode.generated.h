// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CSC546_CSC546GameMode_generated_h
#error "CSC546GameMode.generated.h already included, missing '#pragma once' in CSC546GameMode.h"
#endif
#define CSC546_CSC546GameMode_generated_h

#define CSC546_Source_CSC546_CSC546GameMode_h_9_RPC_WRAPPERS
#define CSC546_Source_CSC546_CSC546GameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define CSC546_Source_CSC546_CSC546GameMode_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACSC546GameMode(); \
	friend struct Z_Construct_UClass_ACSC546GameMode_Statics; \
public: \
	DECLARE_CLASS(ACSC546GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CSC546"), CSC546_API) \
	DECLARE_SERIALIZER(ACSC546GameMode)


#define CSC546_Source_CSC546_CSC546GameMode_h_9_INCLASS \
private: \
	static void StaticRegisterNativesACSC546GameMode(); \
	friend struct Z_Construct_UClass_ACSC546GameMode_Statics; \
public: \
	DECLARE_CLASS(ACSC546GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CSC546"), CSC546_API) \
	DECLARE_SERIALIZER(ACSC546GameMode)


#define CSC546_Source_CSC546_CSC546GameMode_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CSC546_API ACSC546GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACSC546GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CSC546_API, ACSC546GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CSC546_API ACSC546GameMode(ACSC546GameMode&&); \
	CSC546_API ACSC546GameMode(const ACSC546GameMode&); \
public:


#define CSC546_Source_CSC546_CSC546GameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CSC546_API ACSC546GameMode(ACSC546GameMode&&); \
	CSC546_API ACSC546GameMode(const ACSC546GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CSC546_API, ACSC546GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACSC546GameMode)


#define CSC546_Source_CSC546_CSC546GameMode_h_9_PRIVATE_PROPERTY_OFFSET
#define CSC546_Source_CSC546_CSC546GameMode_h_6_PROLOG
#define CSC546_Source_CSC546_CSC546GameMode_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546GameMode_h_9_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546GameMode_h_9_RPC_WRAPPERS \
	CSC546_Source_CSC546_CSC546GameMode_h_9_INCLASS \
	CSC546_Source_CSC546_CSC546GameMode_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CSC546_Source_CSC546_CSC546GameMode_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546GameMode_h_9_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546GameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546GameMode_h_9_INCLASS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546GameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CSC546_Source_CSC546_CSC546GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
