// Fill out your copyright notice in the Description page of Project Settings.

#include "../Public/Neural_Net.h"


// Sets default values
ANeural_Net::ANeural_Net()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CNN = Conv_Net();
}

// Called when the game starts or when spawned
void ANeural_Net::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ANeural_Net::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

TArray<float> ANeural_Net::train(TArray<float> input, TArray<float> output)
{
	return TArray<float>();
}

TArray<float> ANeural_Net::train(UTextureRenderTarget2D * input, TArray<float> output)
{
	return TArray<float>();
}

TArray<float> ANeural_Net::get_output(UTextureRenderTarget2D* input)
{
	std::vector<std::vector<std::vector<float>>> img = std::vector<std::vector<std::vector<float>>>(3);
	TArray<FColor> FormattedImageData;
	try {
		
		FRenderTarget* tex = input->GameThread_GetRenderTargetResource();

		tex->ReadPixels(FormattedImageData);
		for (int i = 0; i < img.size(); i++)
		{
			std::vector<std::vector<float>> tempR;
			std::vector<std::vector<float>> tempG;
			std::vector<std::vector<float>> tempB;
			for (int j = 0; j < tex->GetSizeXY().Y; j++)
			{
				std::vector<float> _tempR;
				std::vector<float> _tempG;
				std::vector<float> _tempB;
				for (int k = 0; k < tex->GetSizeXY().X; k++)
				{
					FLinearColor pixelColor = FormattedImageData[k * tex->GetSizeXY().X + j];

					_tempR.push_back(pixelColor.R / 255);
					_tempG.push_back(pixelColor.G / 255);
					_tempB.push_back(pixelColor.B / 255);
				}
				tempR.push_back(_tempR);
				tempG.push_back(_tempG);
				tempB.push_back(_tempB);
			}
			img[0] = tempR;
			img[1] = tempG;
			img[2] = tempB;
		}
	}
	catch(void*)
	{
		float x = 0;
	}

	return this->vec_to_arr(CNN.get_output(img));
}

template<typename t>
inline TArray<t> ANeural_Net::vec_to_arr(std::vector<t> in)
{
	TArray<t> arr;
	arr.SetNumUninitialized(in.size());
	for (int i = 0; i < in.size(); i++)
		arr[i] = in[i];
	return arr;
}

template<typename t>
inline std::vector<t> ANeural_Net::arr_to_vec(TArray<t> in)
{
	std::vector<t> ret = std::vector<t>(in.Num());
	for (int i = 0; i < ret.size(); i++)
		ret[i] = in[i];
	return ret;
}

