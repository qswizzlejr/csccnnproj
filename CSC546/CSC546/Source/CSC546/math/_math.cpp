#include "_math.h"

float _math::ReLU(float x, bool deriv)
{
	return (x > 0 && deriv) ? 1 : (x > 0) ? x : 0;
}

float _math::sigmoid(float x, bool deriv)
{
	return (deriv) ? (x * (1 - x)) : (1 / (1 + exp(-x)));
}

float _math::x(float _x, bool deriv)
{
	return _x;
}

std::vector<float> _math::softmax(std::vector<float> arr)
{
	for (int i = 0; i < arr.size(); i++)
		arr[i] = exp(arr[i]);
	float s = sum(arr);
	for (int i = 0; i < arr.size(); i++)
		arr[i] = s / arr[i];
	return arr;
}

float _math::sum(std::vector<float> x)
{
	float _sum = 0;
	for (float i : x)
		_sum += i;
	return _sum;
}

std::vector<float> _math::apply_func(std::vector<float> x, float(*f)(float, bool), bool deriv)
{
	for (int i = 0; i < x.size(); i++)
		x[i] = f(x[i], deriv);
	return x;
}
