#pragma once
#include <math.h>
#include <vector>

namespace _math
{
	///math
	//float
	float ReLU(float x, bool deriv = false);
	float sigmoid(float x, bool deriv = false);
	float x(float _x, bool deriv = false);

	//std::vector<float>
	std::vector<float> softmax(std::vector<float>);

	///aux
	//float
	float sum(std::vector<float>);

	//std::vector<float>
	std::vector<float> apply_func(std::vector<float>, float(*f)(float, bool), bool deriv = false);		//apply the specified function to an array
}